FROM python:3.10 as builder

WORKDIR /builder

# install Python requirements for scripts
COPY requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# install content
COPY content ./

# run script for modifying content
COPY modify.py /tmp/modify.py
RUN chmod +x /tmp/modify.py
RUN /tmp/modify.py


FROM nginx:1.23

# install modofied content
COPY --from=builder /builder/out/ ./content/

# install nginx configurations
COPY docker/root.conf /etc/nginx/sites-available/default
COPY docker/ssl.conf /etc/nginx/sites-available/ssl

# entrypoint
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
