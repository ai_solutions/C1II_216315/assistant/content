#!/usr/bin/env python3

from pathlib import Path

from tqdm.auto import tqdm
import moviepy.editor as mp

# TODO: make input and output as argv

content_folder = Path("./content").resolve()

out_folder = Path("./out").resolve()
out_folder.mkdir(exist_ok=True)

# process video
video_folder = content_folder / "video"

target_resolutions = [[1920, 1440], [1280, 960], [640, 480]]

with tqdm(
    desc="processing video",
    total = sum([len(list(top_folder.iterdir())) for top_folder in video_folder.iterdir()])
) as pbar:
    for top_folder in video_folder.iterdir():
        for sub_folder in top_folder.iterdir():
            (out_folder/top_folder.name/sub_folder.name).mkdir(parents=True, exist_ok=True)
            
            files = sorted(list(sub_folder.iterdir()))
            for file in files:
                pbar.desc = f"processing video file: `{file}`"
                pbar.update(1)

                input_file = mp.VideoFileClip(str(file))

                for resolution in target_resolutions:
                    clip = input_file.resize(resolution)

                    # process default to not add anything
                    if file.name.startswith("default"):
                        new_file_name = f"{clip.size[0]}x{clip.size[1]}.mp4"
                    else:
                        new_file_name = f"{file.name}_{clip.size[0]}x{clip.size[1]}.mp4"

                    clip.write_videofile(str(out_folder/top_folder.name/sub_folder.name/new_file_name), verbose=False, logger=None)
